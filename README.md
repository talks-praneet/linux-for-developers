# Title of the talk/workshop: Linux essentials for Developers

### Abstract of the talk/workshop
* Why Linux?
* Where do you find it?
* What is Linux?
    * [Single Unix Specification](https://en.wikipedia.org/wiki/Single_UNIX_Specification)
    * [Filesystem Hierarchy Standard](https://en.wikipedia.org/wiki/Filesystem_Hierarchy_Standard)
    * [Portable Operating System Interface](https://en.wikipedia.org/wiki/POSIX)
    * [Linux Kernel](https://en.wikipedia.org/wiki/Linux_kernel)
    * [Linux Standard Base](https://en.wikipedia.org/wiki/Linux_Standard_Base)
    * [Linux from Scratch](http://www.linuxfromscratch.org/)
* How to navigate the Flavors?
    * [Distributions](https://en.wikipedia.org/wiki/List_of_Linux_distributions) and [DistroWatch](https://distrowatch.com/)
    * [Shells](https://en.wikipedia.org/wiki/Comparison_of_command_shells)
    * [User Interfaces/Window Managers](https://en.wikipedia.org/wiki/Comparison_of_X_window_managers)
* Deep Dive/Hands On w.r.t Developers

### Category of the talk/workshop
Developer tools and automation

### Duration (including Q&A)
3 hrs

### Level of Audience
Intermediate

### Speaker Bio

Praneet is currently the Director of Machine Learning at [CBREX](https://www.cbr.exchange/). 

He is very passionate about technology and has experience building systems across the [SMAC](https://en.wikipedia.org/wiki/Third_platform) stack. 
Earlier in his career, He has been involved with a number of startups and midsize companies, helping the founding teams test their luck in advertising and retail markets at scale.

He also possesses an amateur radio license under the call sign - KJ4IFQ. When he is not tinkering with code or circuits, he is out there enjoying a good long run with his friends. You can follow him on twitter [@growingsmart](https://twitter.com/growingsmart).

### Prerequisites(if any)
Virtual Box or Docker with a linux distribution. Please refer to the [gitlab repo](https://gitlab.com/talks-praneet/linux-for-developers) for more details.

#### Minimal Installation: 

##### Install Docker

https://docs.docker.com/install/

##### Pull the container

sudo docker pull python

##### Run the container

sudo docker run -d -t --name linux-container python:latest

##### SSH into the container

sudo docker exec -it linux-container /bin/bash
